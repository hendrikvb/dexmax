#!/usr/bin/python

# This script is scheduled to run every 10 minutes, displays glucose levels and trends for 60 seconds
# Code part of dexmax - https://gitlab.com/hendrikvb/dexmax - 10 May 2023

# -*- coding:utf-8 -*-
import sys
import os
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)

import time
import requests
import traceback
from pydexcom import Dexcom

import RPi.GPIO as GPIO
from time import sleep, strftime
from datetime import datetime

from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.core.virtual import viewport
from luma.led_matrix.device import max7219
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, CP437_FONT, LCD_FONT

def up_arrow(position, draw_obj):
  draw_obj.point((24,3+position), fill="white")
  draw_obj.point((25,2+position), fill="white")
  draw_obj.point((26,1+position), fill="white")
  draw_obj.point((27,0+position), fill="white")
  draw_obj.point((28,0+position), fill="white")
  draw_obj.point((29,1+position), fill="white")
  draw_obj.point((30,2+position), fill="white")
  draw_obj.point((31,3+position), fill="white")

def down_arrow(position,draw_obj):
  draw_obj.point((24,4-position), fill="white")
  draw_obj.point((25,5-position), fill="white")
  draw_obj.point((26,6-position), fill="white")
  draw_obj.point((27,7-position), fill="white") 
  draw_obj.point((28,7-position), fill="white")
  draw_obj.point((29,6-position), fill="white")
  draw_obj.point((30,5-position), fill="white")
  draw_obj.point((31,4-position), fill="white")

def get_reading():
  dexcom = Dexcom("dexcomuser", "dexcompassword","ous=True") # add ous=True if outside of US
  trends = ["","Rising Quickly","Rising","Rising Slightly","Steady","Falling Slightly","Falling","Falling Quickly","Unable to determine trend","Trend Unavailable"]
  bg = dexcom.get_current_glucose_reading()
  bgl = 0
  bgltime = ""
  bgltrend = 0 
  try:
    bgl = bg.value
    bgltime = bg.time 
    bgltrend = bg.trend
  except TypeError:
    bgl = 0 
    bgltime = ""
    bgltrend = 8
    print("Unknown reading value!")
 
  return str(bgl), str(bgltime), trends[bgltrend]

serial = spi(port=0, device=0, gpio=noop())
device = max7219(serial, width=32, height=8, block_orientation=-90)
device.contrast(5)
virtual = viewport(device, width=32, height=16)

bgl, date, trend = get_reading()

try:
  with canvas(virtual) as draw:
    text(draw, (0, 1), bgl, fill="white", font=proportional(CP437_FONT))
    if trend == "Rising Slightly":
      up_arrow(0, draw)
    if trend == "Rising": 
      up_arrow(0, draw)
      up_arrow(2, draw)
    if trend == "Rising Quickly": 
      up_arrow(0, draw)
      up_arrow(2, draw)
      up_arrow(4, draw)

    if trend == "Falling Slightly":
      down_arrow(0, draw)
    if trend == "Falling":
      down_arrow(0, draw)
      down_arrow(2, draw)
    if trend == "Falling Quickly":
      down_arrow(0, draw)
      down_arrow(2, draw)
      down_arrow(4, draw)

    if trend == "Steady":
      draw.line((24, 3, 31, 3), fill="white")
  time.sleep(60)

except KeyboardInterrupt:
    GPIO.cleanup()
