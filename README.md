# dexmax

## About
DexMax is a simple Python3 script that grab glucose levels from a Dexcom CGM and feeds it to a Max7219 matrix display running on a Raspberry Pi Zero.

## Installation
**First step**: Prepare a working Raspberry Pi. Development and testing is done on a Raspberry Pi Zero W Rev 1.1 (wih GPIO pins). Set up the wireless configuration. Connect the Max7219 to the GPIO pins.

**Second step**: Install supporting Python 3 libraries:
```
apt-get install python3-rpi.gpio python-imaging python-smbus python-dev
pip3 install pydexcom
```

**Third step**:Install the WaveShare e-Paper Python libraries:
```
git clone http://github.com/rm-hull/luma.led_matrix.git
cd luma.led_matrix/
python setup.py  install
```

**Fourth step**: clone the dexmax repository:
```
git clone https://gitlab.com/hendrikvb/dexmax
cd dexmax
```

**Fifth step**: Configure and customize the script to your configuration. In ```dexmax.py```, change the DexCom API settings:
```
dexcom = Dexcom("username", "password","ous=True") # add ous=True if outside of US
```
See instructions on [https://github.com/gagebenne/pydexcom] on getting these credentials.

By default, the values are displayed for 1 minute. To change this, update the ```time.sleep(60)``` line.

## Pictures
![DexMax in a box](dexmax.jpg "DexMax in a box")

## Scheduling
Ideally, schedule the execution with a cronjob to run every 10 minutes (adjust where needed):
```
0,10,20,30,40,50 * * * * python3 /home/user/dexmax.py
```

To use GPIO and SPI access as a regular user, make sure to add the user to the ```spi``` and ```gpio``` usergroups.

## Authors and acknowledgment
Code is based on Max7219 example code. Some code is based on earlier work by me, as part of the dexpi project, located at [https://gitlab.com/hendrikvb/dexpi] and the dexdash project [https://gitlab.com/hendrikvb/dexdash/]

## Contact
The best way to reach out is to send a [Twitter DM @hendrikvb](https://twitter.com/hendrikvb).

## Project status
Initial functional build. Needs work

## License
FREE BUT USE AT OWN RISK! DO NOT USE THIS CODE TO TAKE MEDICAL DECISIONS!
